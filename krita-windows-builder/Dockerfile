#syntax=docker/dockerfile:1.5-labs
#escape=`
FROM mcr.microsoft.com/windows/server:ltsc2022

LABEL org.opencontainers.image.title="Windows Image for Krita builds"
LABEL org.opencontainers.image.authors="dimula73@gmail.com"

ENV chocolateyUseWindowsCompression=false


# Switch to Powershell from here on now that Visual Studio is all happy
SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

# Install chocolatey, then use it to install Git, Python 2 and 3, 7zip and Powershell Core
RUN iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')); `
    choco feature disable --name=showDownloadProgress; `
    choco install -y git python310 ccache ninja 7zip powershell-core cmake; `
    Remove-Item @( 'C:\*Recycle.Bin\S-*' ) -Force -Recurse -Verbose; `
    Remove-Item @( 'C:\Windows\Temp\*', 'C:\Windows\Prefetch\*', 'C:\Documents and Settings\*\Local Settings\temp\*', 'C:\Users\*\Appdata\Local\Temp\*' ) -Force -Recurse -Verbose;

# Make sure Git does not attempt to use symlinks as they don't work well with CMake and co
RUN git config --system core.symlinks false

# Install a couple of Python bindings needed by the CI Tooling
RUN pip install lxml pyyaml python-gitlab packaging; `
    Remove-Item @( 'C:\Windows\Temp\*', 'C:\Windows\Prefetch\*', 'C:\Documents and Settings\*\Local Settings\temp\*', 'C:\Users\*\Appdata\Local\Temp\*' ) -Force -Recurse -Verbose;

ADD llvm-mingw-20220906-ucrt-x86_64.zip.md5sum 'c:\tools\'

SHELL ["cmd.exe", "/S", "/C"]

WORKDIR 'c:\tools\'
RUN curl -SL "https://github.com/mstorsjo/llvm-mingw/releases/download/20220906/llvm-mingw-20220906-ucrt-x86_64.zip" -O && `
    "C:\Program Files\Git\bin\bash.exe" -c "cat llvm-mingw-20220906-ucrt-x86_64.zip.md5sum | tr -d '\r' | md5sum -c -" && `
    7z x llvm-mingw-20220906-ucrt-x86_64.zip && `
    del llvm-mingw-20220906-ucrt-x86_64.zip

# Switch to a cmd.exe shell
# This is needed to allow us to error out easily enough, as Powershell does not consider non-zero exit codes of commands to be fatal errors
SHELL ["cmd.exe", "/S", "/C"]
